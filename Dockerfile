FROM python:3.9-slim

EXPOSE 8000
EXPOSE 8088

COPY src/ src/

WORKDIR src

CMD ["main.py", "--loglevel=DEBUG"]
ENTRYPOINT ["python3"]
