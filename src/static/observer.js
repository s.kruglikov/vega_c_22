const PackageObserver = {
    observerId: undefined,
    intervalCounter: 0,

    up: function(interval) {
        setInterval(this.observerHandler,interval);
        let intervalIndicatorItem = $("#interval_indicator");
        let indicatorRefreshInterval = 1000
        let me = this;
        intervalIndicatorItem && setInterval(function() {
            const leftTime = (interval - indicatorRefreshInterval*me.intervalCounter++)/1000;
            leftTime <= 1 && (me.intervalCounter = 0);
            intervalIndicatorItem.text(leftTime);
        }, indicatorRefreshInterval);
    },

    observerHandler: function() {
        $.ajax({
            url: 'get_settings',
            type: "GET",
            success: function (data) {
                if(data) {
                    Object.entries(data).forEach((field_data,index) => {
                        field = $("#"+field_data[0]);
                        if(field && typeof field.val === 'function' ) {
                            field.val(field_data[1]);
                        }
                        field = $("#exchange_package");
                        if(field_data[0] === "package" && typeof field.text === 'function' ) {
                            field.text(field_data[1]);
                        }
                    });
                }

            },
            error: function (error) {
                console.log('Error '+error);
            }
        });
    }
}



