function start_observer(interval) {
    Object.create(PackageObserver).up(interval || $observer_interval*1000);
}