import logging
from http.server import HTTPServer
from config import HTTP_ADDRESS
from config import HTTP_PORT
from controller.app_controller import IndexPageRequestHandler

HTTPD = HTTPServer((HTTP_ADDRESS, HTTP_PORT), IndexPageRequestHandler)

def shutdown_handler():
    logging.debud('call shutdown HTTP server')
    HTTPD.shutdown()

if __name__ == '__main__':
    try:
        HTTPD.serve_forever()
    except Exception:
        shutdown_handler()

