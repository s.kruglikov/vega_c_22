from view.view_helper import normalize_value as norm_value
from vega_c_22.package import Package


class VegaSettingsAdapter:

    @staticmethod
    def to_model_items(setting_fields: dict) -> dict:
        items = {}
        for name, data in setting_fields.items():
            original_value = data['field_value']
            normal_value = norm_value(original_value)
            description = data['description']
            items[name] = normal_value
            if original_value:
                items[name + "_" + normal_value] = 'selected'
            if description:
                items[name + '_description'] = description
        return items

    @staticmethod
    def to_vega_field(setting_fields: dict, vega_items: dict) -> dict:
        result = {}
        for name, field in setting_fields.items():
            result[name] = {}
            for f_name, f_data in field.items():
                result[name][f_name] = f_data
            if name in vega_items:
                result[name]['field_value'] = vega_items[name]
        return result

    @staticmethod
    def to_field_value(setting_fields: dict, vega_items: dict) -> dict:
        result = {}
        for name, field in setting_fields.items():
            result[name] = ""
            if name in vega_items:
                result[name] = vega_items[name]
        return result


def to_package(view, vega_settings) -> Package:
    vega_items = VegaSettingsAdapter.to_vega_field(vega_settings, view.get_model_items())
    return Package(0, vega_items)
