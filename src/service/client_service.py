import logging
import socket


class ClientService:

    def __init__(self, host: str, port: int) -> None:
        self.vega_address = (host, port)
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    def send(self, data):
        logging.debug("try connecting to: %s:%s", self.vega_address[0], str(self.vega_address[1]))
        self.socket.connect(self.vega_address)
        logging.debug("send data to vega %s", data)
        self.socket.send(data)
        logging.debug("closing connection")
        self.socket.close()
        logging.debug("connection is closed")
