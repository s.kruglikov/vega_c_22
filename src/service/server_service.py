import logging
import signal
import socket
import threading
from datetime import datetime
from service.synchroize_helper import synchronized
from vega_c_22.package import Package


class ServerService:

    def __init__(self, host: str = None, port: int = None, buffer_size: int = 1024) -> None:
        self.socket = None
        self.listening_thread = None
        self.server_up = False
        self.address = (host, port)
        self.un_bind = True
        self.last_received_data = None
        self.connections = {}
        self.buffer_size = buffer_size
        self.sigint_prev_handler = None
        self.sigter_prev_handler = None
        package = Package(0)
        self.last_package = {'client': 'initial value', 'timestamp': datetime.now(), 'package': package}

    def up(self):
        try:
            self.socket = socket.create_server(address=self.address)
            self.socket.listen(5)
            self.server_up = True
            self.sigint_prev_handler = signal.signal(signal.SIGINT, self.shutdown_handler)  # KeyboardInterrupt
            self.sigter_prev_handler = signal.signal(signal.SIGTERM, self.shutdown_handler)  # Kill
            self.listening_thread = threading.Thread(target=self.create_listening_thread, args=())
            self.listening_thread.start()
            logging.info('Socket now listening on "%s:%s"', self.address[0], self.address[1])
        except Exception as exp:
            self.server_up = False
            logging.error(exp)

    def get_host(self) -> str:
        return self.address[0]

    def get_port(self) -> int:
        return self.address[1]

    def create_listening_thread(self):
        while True:
            try:
                (connection, from_address) = self.socket.accept()
                self.connections[from_address] = connection
                logging.info("The client %s has been connected", from_address)
                client_connection_thread = threading.Thread(target=self.client_thread, args=from_address)
                client_connection_thread.start()
                logging.debug("Clients are connected now: %s", (threading.active_count() - 1))
            except Exception:
                break
        logging.debug('Server socket has finished accepting')

    def client_thread(self, host, port) -> None:
        from_address = (host, port)
        client_socket = self.connections[from_address]
        try:
            while True:
                received_data = client_socket.recv(self.buffer_size)
                if len(received_data) > 0:
                    logging.debug("saving received package...")
                    self.save_package(from_address, datetime.now(), received_data)
                    logging.debug("a package has been received")
                else:
                    break
        except Exception as exp:
            logging.error(exp)
        client_socket.close()
        self.connections.pop(from_address)
        logging.debug("a client socket %s has been closed", from_address)

    @synchronized
    def save_package(self, client: list = None, timestamp: datetime = None, data: bytes = None) -> None:
        if data is not None:
            logging.info("received package data: '%s'", data)
            package = Package.deserialize(data)
            logging.debug("deserialized package data: %s", package)
            self.last_package = {'client': client, 'timestamp': timestamp, 'package': package}

    @synchronized
    def get_last_package(self) -> dict:
        return self.last_package

    def is_up(self) -> bool:
        return self.server_up

    def shutdown(self) -> None:
        if self.server_up:
            logging.info("The server is shutting down...")
            if len(self.connections):
                logging.debug("closing client accepted sockets")
                [client_socket.close() for client_socket in self.connections.items()]
                self.connections.clear()
            logging.debug("closing server socket")
            self.socket.shutdown(socket.SHUT_RDWR)
            self.server_up = False
        if self.sigint_prev_handler is not None:
            signal.signal(signal.SIGINT, self.sigint_prev_handler)
            self.sigint_prev_handler = None
        if self.sigter_prev_handler is not None:
            signal.signal(signal.SIGTERM, self.sigter_prev_handler)
            self.sigter_prev_handler = None

    def shutdown_handler(self, signum, frame) -> None:
        logging.info("call shutdown VEGA server")
        self.shutdown()
