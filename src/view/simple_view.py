from view.view_helper import normalize_value as norm_val
from view.view_model import SimpleViewModel


class SimpleViewObject:
    def __init__(self, items: dict = None, view_model: SimpleViewModel = None) -> None:
        self.view_model = view_model if view_model else SimpleViewModel()
        if items:
            for item in items.items():
                self.view_model.register(item[0], item[1])

    def get_view_model(self) -> SimpleViewModel:
        return self.view_model

    def apply_parameters(self, params: dict, apply_new: bool = True) -> None:
        for k, v in params.items():
            norm_k = norm_val(k)
            norm_v = norm_val(v)
            if apply_new:
                self.view_model.apply({norm_k: norm_v})
                if v:
                    self.view_model.apply({norm_k + '_' + norm_v: 'selected'})
            elif self.get_model_item(norm_k) is not None:
                self.view_model.apply({norm_k: norm_v})
                if v:
                    self.view_model.apply({norm_k + '_' + norm_v: 'selected'})

    def get_model_items(self) -> dict:
        return self.view_model.get_items()

    def get_model_item(self, item_name: str) -> dict:
        result = None
        model_items = self.view_model.get_items()
        if item_name in model_items:
            result = model_items[item_name]
        return result

    def get_items(self, item_name: str, default_value=None) -> dict:
        items = self.get_model_items()
        return items[item_name] if item_name in items else default_value
