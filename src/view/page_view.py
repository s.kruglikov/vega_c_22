import logging
from view.simple_view import SimpleViewObject
from view.view_helper import normalize_value as norm_val
from vega_c_22.package import Package

class PageViewObject(SimpleViewObject):

    def __init__(self, items=None, view_model=None):
        super().__init__(items, view_model)

    def set_mode(self, mode_config: dict) -> None:
        default_config = {
            'mode': 'server',
            'class_visible': 'class_visible',
            'class_hidden': 'class_hidden'
        }
        config = default_config if mode_config is None else default_config | mode_config
        sr = config['class_visible'] if config['mode'] == 'server' else config['class_hidden']
        cl = config['class_hidden'] if config['mode'] == 'server' else config['class_visible']
        self.apply_parameters({
            'class_visibility_for_client': cl,
            'class_visibility_for_server': sr,
            'mode_value': config['mode']
        })

    def set_package(self, package: Package) -> None:
        delimiter = self.get_items('field_byte_delimiter', '')
        package_bytes = package.serialize()
        package_formatted = delimiter + package_bytes.hex(':').replace(':', delimiter)
        self.apply_parameters({'field_binary_package': package_formatted})
        deserialized_package = package.deserialize(package_bytes)
        logging.debug("deserialized_package: %s", deserialized_package)

    @staticmethod
    def parameters_to_items(params: dict) -> dict:
        result = {}
        if params:
            for k, v in params.items():
                result[norm_val(k)] = norm_val(v)
        return result