class SimpleViewModel:
    def __init__(self):
        self.items = {}

    def register(self, name, value=None) -> None:
        if not (len(name) > 0):
            raise Exception('item_name should not empty')
        self.items[name] = value

    def apply(self, model_item) -> None:
        for key, value in model_item.items():
            self.register(key, value)

    def get_items(self) -> dict:
        return self.items

