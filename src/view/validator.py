
class NumberRangeValidator:

    def __init__(self, lower_bound, upper_bound, mandatory):
        self.lower_bound = lower_bound
        self.upper_bound = upper_bound
        self.mandatory = mandatory

    def validate(self, value):
        result = False
        if value:
            norm_value = value.strip()
            try:
                num_value = int(norm_value)
                if self.lower_bound <= num_value <= self.upper_bound:
                    result = True
            except ValueError:
                pass
        elif not self.mandatory:
            result = True

        return result