from string import Template
from collections import defaultdict

class TemplateObject:
    def __init__(self, view):
        self.view = view

    def apply_view_model(self, content):
        view_items = defaultdict(lambda: "")
        view_items.update(self.view.get_model_items())
        return Template(content).substitute(view_items)
