
def normalize_value(value):
    if isinstance(value, bytes):
        n_val = value.decode("utf-8")
    elif isinstance(value, list):
        n_val = normalize_value(value[0])
    else:
        n_val = str(value)
    return n_val