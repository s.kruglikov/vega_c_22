import logging
import mimetypes
from os.path import exists


class ResourceHelper:

    @staticmethod
    def get_resource_mimetype(resource_name):
        resource_parts = resource_name.split('?')
        return mimetypes.guess_type(resource_parts[-1])[0]

    @staticmethod
    def get_resource_name(resource_path):
        return resource_path.split("/")[-1]

    @staticmethod
    def get_resource_content(resource):
        path = resource['path']
        content = ""
        if exists(path):
            if resource['request_type'] == 'template':
                file = open(path, 'r', encoding="utf-8")
            else:
                file = open(path, 'rb')
            content = file.read()
            file.close()
        else:
            logging.info("not found: '%s'", path)
        return content

    @staticmethod
    def get_resource_data(path, request_type, mimetype):
        path = path.split("?")[0]
        name = ResourceHelper.get_resource_name(path)
        request_type = request_type if request_type is not None else name.find(".htm") >= 0
        return {
            'name': name,
            'request_type': request_type,
            'mimetype': mimetype if mimetype is not None else ResourceHelper.get_resource_mimetype(name),
            'path': path.removeprefix("/")
        }
