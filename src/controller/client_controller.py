import logging
import threading
from controller.controller_objects import ResponseObject
from config import SERVER_SERVICE
from view.page_view import PageViewObject
from service.client_service import ClientService
from vega_c_22.package import Package


class ClientController:
    @staticmethod
    def to_client_action(view: PageViewObject) -> ResponseObject:
        if SERVER_SERVICE.is_up():
            logging.debug("server_service.shutdown")
            SERVER_SERVICE.shutdown()
            logging.debug("after shutdown threading.active_count %s", threading.active_count())
        view.apply_parameters({'disabled': None})
        return ResponseObject('server' if SERVER_SERVICE.is_up() else 'client', None)

    @staticmethod
    def send_action(view: PageViewObject, package: Package) -> ResponseObject:
        package_bytes = package.serialize()
        server_host = view.get_model_item('server_host')
        if server_host and len(server_host.strip()) > 0:
            server_port = view.get_model_item('server_port')
            if server_port is None:
                server_port = view.get_model_item('server_port_default')
            client_service = ClientService(server_host, int(server_port))
            try:
                client_service.send(package_bytes)
                result = 'пакет: {} отправлен'.format(package_bytes.hex())
            except Exception as exp:
                logging.error(exp)
                result = exp
            view.apply_parameters({'exchange_package': result})
        else:
            view.apply_parameters({'exchange_package': 'укажите адрес сервера'})
        return ResponseObject('client', None)
