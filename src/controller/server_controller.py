import json
import logging
import threading
from config import SERVER_SERVICE
from config import VEGA_SETTINGS
from controller.controller_objects import ResponseObject
from view.page_view import PageViewObject
from vega_c_22.package_adapter import PackageAdapter
from vega_adapter import VegaSettingsAdapter


class ServerController:

    @staticmethod
    def to_server_action(view: PageViewObject) -> ResponseObject:
        response_data = None
        if not SERVER_SERVICE.is_up():
            logging.debug("before server up threading.active_count:%s", threading.active_count())
            SERVER_SERVICE.up()
            logging.debug("after server up threading.active_count:%s", threading.active_count())
        if SERVER_SERVICE.is_up():
            view.apply_parameters({
                'field_binary_package': 'ожидание пакетов...',
                'start_observer': ' start_observer(); console.log("start_observer....."); ',
                'server_listening': SERVER_SERVICE.get_host() + ':' + str(SERVER_SERVICE.get_port()),
                'disabled': 'disabled="true"'
            })
        return ResponseObject('server' if SERVER_SERVICE.is_up() else 'client', response_data)

    @staticmethod
    def do_read_settings_action() -> ResponseObject:
        response_data = SERVER_SERVICE.get_last_package()
        package_as_dict = PackageAdapter.get_package_as_dict(response_data['package'])
        vega_fld = VegaSettingsAdapter.to_field_value(VEGA_SETTINGS.get_firmware(), package_as_dict)
        vega_fld['package'] = " текущий пакет: {}".format(package_as_dict['package'])
        return ResponseObject('server', json.dumps(vega_fld))
