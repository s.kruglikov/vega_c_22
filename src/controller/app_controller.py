import http.server as server
import logging
import re
from datetime import datetime
from urllib.parse import parse_qs

from config import SERVER_SERVICE
from config import VEGA_SETTINGS
from config import OBSERVER_INTERVAL
from config import ROUTES

from vega_adapter import VegaSettingsAdapter
from vega_adapter import to_package
from resource_helper import ResourceHelper
from view.page_view import PageViewObject
from view.template import TemplateObject
from controller.server_controller import ServerController
from controller.client_controller import ClientController
from controller.controller_objects import ResponseObject


class IndexPageRequestHandler(server.BaseHTTPRequestHandler):
    CACHE_VALUE = datetime.now().strftime("%H%M%S%f")

    def log_message(self, message_format, *args):
        if len(args) == 1:
            logging.debug('"%s"', args[0])
        elif len(args) == 2:
            logging.debug('"%s" %s', args[0], args[1])
        elif len(args) == 3:
            logging.debug('"%s" %s %s', args[0], args[1], args[2])

    def do_GET(self) -> None:
        resource = self.get_resource()
        view = None
        if resource['request_type'] == 'template':
            view = self.get_view()
            self.do_template_action(view)
        elif resource['request_type'] == 'ajax':
            resource['body'] = self.do_ajax_action().get_body()
        self.write_response(resource, view)

    def do_POST(self) -> None:
        headers = self.headers
        length_body = int(headers['Content-Length'])
        post_data = parse_qs(self.rfile.read(length_body))
        resource = self.get_resource()
        view = None
        if resource['request_type'] == 'template':
            view = self.get_view()
            request_items = PageViewObject.parameters_to_items(post_data)
            view.apply_parameters(request_items)
            self.do_template_action(view)
        self.write_response(resource, view)

    def get_view(self) -> PageViewObject:
        view = PageViewObject({'field_binary_package': '',
                               'disable_cache': self.CACHE_VALUE,
                               'server_host_default': SERVER_SERVICE.get_host(),
                               'server_port_default': str(SERVER_SERVICE.get_port()),
                               'observer_interval': str(OBSERVER_INTERVAL)
                               })
        return view

    def do_ajax_action(self) -> ResponseObject:
        action = self.path.removeprefix('/')
        response = None
        if action == 'get_settings':
            response = ServerController.do_read_settings_action()
        return response

    def do_template_action(self, view: PageViewObject) -> ResponseObject:
        vega_fld = VegaSettingsAdapter.to_vega_field(VEGA_SETTINGS.get_firmware(), view.get_model_items())
        VEGA_SETTINGS.set_settings(vega_fld)
        vega_model_items = VegaSettingsAdapter.to_model_items(VEGA_SETTINGS.get_settings())
        mode = 'server' if SERVER_SERVICE.is_up() else 'client'
        view.apply_parameters(vega_model_items, True)
        view.apply_parameters({'exchange_package': ''})
        action = self.path.removeprefix('/')
        if action == 'encode':
            view.set_package(to_package(view, VEGA_SETTINGS.get_settings()))
            response = ResponseObject(mode, None)
        elif action == 'server':
            response = ServerController.to_server_action(view)
        elif action == 'client':
            response = ClientController.to_client_action(view)
        elif action == 'send':
            package = to_package(view, VEGA_SETTINGS.get_settings())
            response = ClientController.send_action(view, package)
        else:
            response = ResponseObject(mode, None)
            vega_model_items = VegaSettingsAdapter.to_model_items(VEGA_SETTINGS.get_firmware())
            view.apply_parameters(vega_model_items)
        view.set_mode(response.get_response())
        return response

    def write_response(self, resource: dict, view: PageViewObject) -> None:
        self.set_response_header(resource)
        if 'body' in resource:
            resource_content = resource['body']
        else:
            resource_content = ResourceHelper.get_resource_content(resource)
            if resource['request_type'] == 'template':
                template = TemplateObject(view)
                try:
                    resource_content = template.apply_view_model(resource_content)
                except Exception as e:
                    logging.error("error: %s", resource['path'])
                    raise e
        if not isinstance(resource_content, bytes):
            resource_content = bytes(resource_content, 'utf-8')
        self.wfile.write(resource_content)

    def set_response_header(self, resource):
        self.send_response(200)
        self.send_header('Content-type', resource['mimetype'])
        self.end_headers()

    def get_resource(self):
        route = self.process_route(self.path)
        return ResourceHelper.get_resource_data(path=route['path'], request_type=route['request_type'],
                                                mimetype=route['mimetype'])

    def process_route(self, path):
        result = {
            'path': path,
            'request_type': None,
            'mimetype': None
        }
        if path in ROUTES:
            route = ROUTES[self.path]
            result['path'] = route['path'] if route['path'] is not None else self.path
            result['request_type'] = route['request_type']
            result['mimetype'] = route['mimetype'] if 'mimetype' in route else None
        else:
            for key, value in ROUTES.items():
                if key.find('regexp') >= 0 and re.search(value['path'], self.path):
                    result['path'] = self.path
                    result['request_type'] = value['request_type']
                    result['mimetype'] = value['mimetype'] if 'mimetype' in value else None
        return result
