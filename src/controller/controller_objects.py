class ResponseObject:
    def __init__(self, mode: str, body: str) -> None:
        self.mode = mode
        self.body = body

    def get_response(self) -> dict:
        return {'mode': self.mode, 'body': self.body}

    def get_mode(self) -> str:
        return self.mode

    def get_body(self) -> str:
        return self.body