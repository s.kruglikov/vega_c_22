import socket
import argparse
import logging
from service.server_service import ServerService as Server
from vega_c_22.settings import VegaSettings


parser = argparse.ArgumentParser(description='The CLI parameters')
parser.add_argument('--loglevel', type=str, default='INFO', nargs='?',
                    help='possible values:[INFO(default)|DEBUG|ERROR|WARNING]')
parser.add_argument('--httpport', type=str, default='8000', nargs='?',
                    help='port for http server')
parser.add_argument('--vegaport', type=str, default='8088', nargs='?',
                    help='port for vega server')
parser.add_argument('--intervalreading', type=str, default='15', nargs='?',
                    help='interval refresh http page in server mode in seconds')

arguments = vars(parser.parse_args())

logging.basicConfig(format='%(levelname)s:%(message)s',
                    level=arguments['loglevel'])

HTTP_ADDRESS = ''
HTTP_PORT = int(arguments['httpport'])
VEGA_SERVER_HOST_DEFAULT = 'localhost'
try:
    VEGA_SERVER_HOST_DEFAULT = socket.gethostname()
except Exception:
    pass

VEGA_SERVER_PORT_DEFAULT = int(arguments['vegaport'])
OBSERVER_INTERVAL = int(arguments['intervalreading'])  # seconds
RECV_BUFSIZE = 1024  # max of data to be received by socket


ROUTES = {
    '/': {'path': 'template/index.html', 'request_type': 'template'},
    '/encode': {'path': 'template/index.html', 'request_type': 'template'},
    '/server': {'path': 'template/index.html', 'request_type': 'template'},
    '/client': {'path': 'template/index.html', 'request_type': 'template'},
    '/send': {'path': 'template/index.html', 'request_type': 'template'},
    '/get_settings': {'path': None, 'request_type': 'ajax', 'mimetype': 'application/json'},
    'regexp1': {'path': '(app\\.js)', 'request_type': 'template'}
}

# initial application section
VEGA_SETTINGS = VegaSettings()
SERVER_SERVICE = Server(VEGA_SERVER_HOST_DEFAULT, VEGA_SERVER_PORT_DEFAULT, RECV_BUFSIZE)

