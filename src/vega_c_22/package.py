from __future__ import annotations
from vega_c_22.package_parameter_type_field import ParameterTypeField
from vega_c_22.package_parameter_field import ParameterPackageField
from vega_c_22.package_adapter import PackageFieldAdapter as FieldAdapter


class Package:
    def __init__(self, package_type: ParameterTypeField = None, init_fields: list[dict] = None) -> None:
        self.package_type = ParameterTypeField(package_type) if package_type is not None else package_type
        self.fields = []
        if init_fields is not None:
            for fld_id, fld in init_fields.items():
                if fld.get('packageable'):
                    bytes_value = FieldAdapter.get_as_bytes(fld['field_value'], fld['field_length'], fld['is_signed'])
                    packaged_field = ParameterPackageField(fld['field_id'], bytes_value, fld['field_length'],
                                                           fld['is_signed'])
                    self.fields.append(packaged_field)

    def __str__(self) -> str:
        return "Package:\n type: {},\n fields: {}\n".format(self.package_type, self.fields)

    def __repr__(self) -> str:
        return self.__str__()

    def get_fields(self) -> list[ParameterPackageField]:
        return self.fields

    def get_type(self) -> ParameterTypeField:
        return self.package_type

    def set_type(self, type_field: ParameterTypeField) -> None:
        self.package_type = type_field

    def add_field(self, field: ParameterPackageField) -> None:
        self.fields.append(field)

    def serialize(self) -> bytes:
        result = self.package_type.serialize()
        for field in self.fields:
            if field.serializable():
                result += field.serialize()
        return result

    @staticmethod
    def deserialize(package_bytes: bytes) -> Package:
        bytes_length = len(package_bytes)
        package = Package()
        index = 0
        parameter_type_field = ParameterTypeField.deserialize(package_bytes[index:])
        package.set_type(parameter_type_field)
        index += parameter_type_field.type_field_length
        while index < bytes_length:
            field = ParameterPackageField.deserialize(package_bytes[index:])
            package.add_field(field)
            index += 3 + int(field.field_length)
        return package
