from __future__ import annotations
from vega_c_22.package_adapter import PackageFieldAdapter as FieldAdapter


class ParameterTypeField:
    type_field_length = 1
    is_signed = False

    def __init__(self, package_type) -> None:
        if isinstance(package_type, bytes):
            self.package_type = package_type
        else:
            self.package_type = FieldAdapter.get_as_bytes(package_type, self.type_field_length, self.is_signed)

    def serialize(self) -> bytes:
        return self.package_type

    @staticmethod
    def deserialize(type_field_byte: bytes) -> ParameterTypeField:
        type_field_value = type_field_byte[:ParameterTypeField.type_field_length]
        return ParameterTypeField(type_field_value)

    def __str__(self) -> str:
        return "package_type[value:{},length:{}]".format(self.package_type, ParameterTypeField.type_field_length)

    def __repr__(self) -> str:
        return self.__str__()
