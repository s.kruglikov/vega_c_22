import logging


class PackageFieldAdapter:
    @staticmethod
    def get_as_int(value, is_signed: bool) -> int:
        if value is None or value == 'None':
            result = None
        elif isinstance(value, bytes):
            result = int.from_bytes(value, 'little', signed=is_signed)
        else:
            result = int(value)

        return result

    @staticmethod
    def get_value_as_str(value, is_signed: bool) -> str:
        v_int = PackageFieldAdapter.get_as_int(value, is_signed)
        return str(v_int) if v_int is not None else None

    @staticmethod
    def get_as_bytes(value, length_in_bytes: int, is_signed: bool) -> bytes:
        if value is None or value == 'None':
            result = None
        elif isinstance(value, str):
            result = int(value).to_bytes(length=length_in_bytes, byteorder='little', signed=is_signed)
        else:
            result = value.to_bytes(length=length_in_bytes, byteorder='little', signed=is_signed)
        return result


class PackageAdapter:
    @staticmethod
    def get_package_as_dict(package) -> dict:
        result = {"package": package.serialize().hex()}
        for field in package.get_fields():
            field_data = field.get_data()
            result['field_'+str(field_data['id'])] = PackageFieldAdapter.get_value_as_str(field_data['value'],
                                                                                 field_data['is_signed'])
        return result
