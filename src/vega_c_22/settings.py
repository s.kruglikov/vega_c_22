class VegaSettings:
    def __init__(self, settings_fields=None) -> None:
        if settings_fields is not None:
            copy = VegaSettings.make_copy_settings(settings_fields)
        else:
            copy = VegaSettings.get_firmware()
        self.settings_fields = copy

    def get_settings(self) -> dict:
        return self.settings_fields

    def set_settings(self, settings_fields: dict) -> dict:
        for name, value in settings_fields.items():
            if 'field_value' in value:
                self.set_field_value(name, value['field_value'])

    def set_field_value(self, field_name: str, field_value: str) -> None:
        if field_name in self.settings_fields:
            self.settings_fields[field_name]['field_value'] = field_value

    def reset_to_firmware_settings(self) -> None:
        copy = firmware_setting_fields.copy()
        self.settings_fields = copy

    @staticmethod
    def make_copy_settings(source: dict) -> dict:
        result = {}
        for name, value in source.items():
            result[name] = {}
            for f_name, f_value in value.items():
                result[name][f_name] = f_value
        return result

    @staticmethod
    def get_firmware() -> dict:
        return VegaSettings.make_copy_settings(firmware_setting_fields)


firmware_setting_fields = {
    "field_4": {"field_id": 4, "field_value": None, "field_length": 1, "is_signed": False,
                "description": "Запрашивать подтверждение", "packageable": True},
    "field_12": {"field_id": 12, "field_value": None, "field_length": 4, "is_signed": False,
                 "description": "Режим входа 1", "packageable": True},
    "field_13": {"field_id": 13, "field_value": None, "field_length": 2, "is_signed": False,
                 "description": "Режим входа 2", "packageable": True},
    "field_14": {"field_id": 14, "field_value": None, "field_length": 1, "is_signed": False,
                 "description": "Режим входа 3", "packageable": True},
    "field_15": {"field_id": 15, "field_value": None, "field_length": 1, "is_signed": False,
                 "description": "Режим входа 4", "packageable": True},
    "field_16": {"field_id": 16, "field_value": None, "field_length": 1, "is_signed": False,
                 "description": "Период передачи данных", "packageable": True},
    "field_49": {"field_id": 49, "field_value": None, "field_length": 1, "is_signed": False,
                 "description": "Период сбора данных", "packageable": True},
    "field_55": {"field_id": 55, "field_value": None, "field_length": 2, "is_signed": True,
                 "description": "Часовой пояс, в минутах", "packageable": True},
    "field_78": {"field_id": 78, "field_value": None, "field_length": 1, "is_signed": False,
                 "description": "Период накопления данных привыходе температуры за пороги", "packageable": True},
    "field_79": {"field_id": 79, "field_value": None, "field_length": 1, "is_signed": False,
                 "description": "Отправлять данные немедленно при выходе температуры за пороги", "packageable": True},
    "field_80": {"field_id": 80, "field_value": None, "field_length": 1, "is_signed": True,
                 "description": "Нижний порог температуры", "packageable": True},
    "field_81": {"field_id": 81, "field_value": None, "field_length": 1, "is_signed": True,
                 "description": "Верхний порог температуры", "packageable": True}
}
