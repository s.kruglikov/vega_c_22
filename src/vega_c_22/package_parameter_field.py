from vega_c_22.settings import firmware_setting_fields as firmware
from vega_c_22.package_adapter import PackageFieldAdapter as FieldAdapter


class ParameterPackageField:
    field_id = None
    is_signed_type = None
    field_length = None
    field_value = None

    def __init__(self, id: int, value: str, length: int, signed: bool) -> None:
        self.field_id = id
        self.is_signed_type = signed
        self.field_length = length
        self.field_value = value

    def __str__(self) -> str:
        return "package_field[id:{},value:{},length:{},is_signed:{}]".format(
            self.field_id, self.field_value, self.field_length, self.is_signed_type)

    def __repr__(self) -> str:
        return self.__str__()

    def get_data(self) -> dict:
        return {"id": self.field_id,
                "value": self.field_value,
                "length": self.field_length,
                "is_signed": self.is_signed_type}

    def serializable(self):
        return self.field_value != 'None' and self.field_value

    def serialize(self) -> bytes:
        if self.field_value:
            field_id = FieldAdapter.get_as_bytes(self.field_id, 2, False)
            field_length = FieldAdapter.get_as_bytes(self.field_length, 1, False)
            result = field_id + field_length + self.field_value
        else:
            result = None
        return result

    @staticmethod
    def deserialize(field_bytes: bytes):
        field_id = FieldAdapter.get_value_as_str(field_bytes[:2], False)
        field_length = FieldAdapter.get_as_int(field_bytes[2:3], False)
        field_value = field_bytes[3:3 + field_length]
        is_signed_type = firmware['field_' + field_id]['is_signed']
        return ParameterPackageField(field_id, field_value, str(field_length), is_signed_type)
